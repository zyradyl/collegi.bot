require './lib/events/mentions'

# Extend the DiscordRB Event Container to set up hooks for when mentions are
# made in a Discord chat.
module MentionHandler
  include Mentions
  extend Discordrb::EventContainer

  mention do |event|
    # Don't process the message if it originates from Seara
    Mentions.process(event) unless event.message.from_bot?
  end
end
