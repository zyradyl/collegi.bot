# Commands responsible for accessing the minecraft server through discord.
module MinecraftCommands
  include ChatUtils

  # Spit out an error message.
  def self.cmd_error(event)
    SEARA.send_message(                                 \
      event.channel.id,                                 \
      CONFIG['messages']['minecraft_commands']['error'] \
    )
  end

  # Spit out a help message.
  def self.cmd_help(event)
    event.message.delete
    CONFIG['messages']['minecraft_commands']['help'].each do |_key, value|
      ChatUtils.temporary(event, value)
    end
  end

  # Process the command message.
  def self.process(event)
    return unless CONFIG['bot']['privileged-users'].include?(event.user.id)
    command = event.content.strip.split(' ')
    case command[1]
    when 'help'
      cmd_help(event)
    else
      cmd_error(event)
    end
  end
end
