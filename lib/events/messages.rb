module Messages
  include ChatUtils
  include PreFilter

  def self.trigger(event)
    if ( ( event.message.from_bot?               && \
           event.message.content.include?(">")   && \
          !event.message.content.include?("<") ) || \
          !event.message.from_bot?)
      ai_says = WITAI.converse(SecureRandom.uuid, "#{event.message.content}", {})
      if ai_says['confidence'] > 0.75
        ChatUtils.responder(event, ai_says['msg'])
      end
    elsif ( event.message.from_bot? && event.message.channel.id.to_s.eql?(CONFIG['channels']['bot_monitoring']))
      if event.message.content.include?("just joined the server")
        content = event.message.content.split('|')
        ChatUtils.logging(event, 'mc_login', content[0])
        event.message.delete
      elsif event.message.content.include?("just left the server")
        content = event.message.content.split('|')
        ChatUtils.logging(event, 'mc_logoff', content[0])
        event.message.delete
      elsif event.message.content.include?("Server has started.")
        ChatUtils.logging(event, 'mc_started')
        event.message.delete
      elsif event.message.content.include?("Server has stopped.")
        ChatUtils.logging(event, 'mc_stopped')
        event.message.delete
      end
    end
  end

  def self.process(event)
    # Prevent Message Doubling
    if event.message.mentions.empty?
      run_check(event)
    else
      event.message.mentions.each do |mention|
        if mention.current_bot?
          return 0
        end
      end
      run_check(event)
    end
  end

  def self.run_check(event)
    check_result = PreFilter.check_message(event)
    case check_result
    when 0
      return
    when 1
      return
    when 2
      return
    when 3
      return
    when 9
      trigger(event)
    end
  end

end
